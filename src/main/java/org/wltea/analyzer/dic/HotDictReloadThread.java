package org.wltea.analyzer.dic;

import org.apache.logging.log4j.Logger;
import org.wltea.analyzer.help.ESPluginLoggerFactory;
import org.wltea.analyzer.help.Sleep;
import org.wltea.analyzer.help.Sleep.Type;

public class HotDictReloadThread implements Runnable {

	private static final Logger logger = ESPluginLoggerFactory.getLogger(Monitor.class.getName());
 
	 @Override
	    public void run() {
	        while (true) {
	        	Sleep.sleep(Type.MIN, 5);
	            logger.info("[==============]reload hot dict from mysql......");
	            Dictionary.getSingleton().reLoadMainDict();
	        }
	    }
}
